$(document).ready(function(){
    const tbodyText = `
                Acme	Yes	Yes	Yes	Yes	Yes	No
        AkelPad	Plug-in	Yes	Yes	Yes	Yes	Yes
        Alphatk	Yes	Yes	Yes	Yes	Yes	Yes
        Atom	Yes	Yes	Yes	Yes	Yes	Plug-in
        BBEdit	Yes	Yes	Yes	Yes	Yes	Yes
        Bluefish	Yes	Yes	Yes	Yes	Yes	No
        Brackets	Plug-in	Yes	Plug-in	No	Yes	Yes
        Coda	Yes	Yes	Yes	Yes	Yes	Yes
        ConTEXT	No	Partial	Partial	Yes	Yes	Yes
        Diakonos	Yes	Yes	No	No	Yes	Yes
        ed	No	Yes	No	No	No	No
        EditPlus	Yes	Yes	Yes	Yes	Yes	Yes
        Editra	Yes	Yes	?	?	?	Yes
        EmEditor	Yes	Yes	Yes	Yes	Yes	Yes
        Geany	Plug-in	Yes	Yes	Yes	Yes	Yes
        gedit	Yes	Plug-in	Yes	Yes	Yes	Plug-in
        GNU Emacs	Plug-in	Yes	Yes	Yes	Yes	Yes
        Gobby	No	No	Partial	Partial	No	No
        JED	Yes	Yes	Yes	Yes	Yes	Yes
        JOVE	Yes	Yes	No	No	Yes	Yes
        Kate	Yes	Yes	Yes	Yes	Yes	Yes
        KEDIT	No	Yes	No	Yes	Yes	Yes
        Komodo Edit	Yes	Yes	Yes	Yes	Yes	Yes
        KWrite	Yes	Yes	Yes	Yes	Yes	Yes
        LE	No	Yes	No	Yes	Yes	Yes
        Light Table	Plug-in	?	No	No	Yes	?
        Metapad	Partial	No	Yes	Yes	Yes	No
        mg	No	Yes	No	Partial	Yes	No
        MinEd	No	Yes	Yes	Yes	No	Yes
        MS-DOS Editor	No	No	No	Yes	No	No
        Nano	Yes	Yes	No	Yes	Yes	No
        ne	No	Yes	No	No	Yes	Yes
        NEdit	Plug-in	Yes	No	Yes	Yes	Yes
        Notepad	No	No	No	No	No	No
        Notepad++	Yes	Yes	Yes	Yes	Yes	Yes
        Notepad2	No	Limited	Yes	Yes	Yes	Yes
        NoteTab	Yes	Yes	Yes	Yes	Yes	Yes
        nvi	No	Yes	No	No	Yes	?
        Peppermint	Yes	Yes	Yes	Yes	Yes	Yes
        Pico	Yes	No	No	No	No	No
        PolyEdit	Yes	Yes	Yes	Yes	Yes	Yes
        Programmer's Notepad	No	Yes	Yes	Yes	Yes	Yes
        PSPad	Yes	Yes	Yes	Yes	Yes	Yes
        Q10	Yes	?	?	?	?	?
        RText	Yes	Yes	Yes	Yes	Yes	No
        SciTE	No	Limited	No	Yes	Yes	Yes
        SlickEdit	Yes	Yes	Yes	Yes	Yes	Yes
        Smultron	Yes	Yes	Yes	Yes	Yes	Yes
        Source Insight	No	Yes	No	Yes	Yes	Yes
        SubEthaEdit	Yes	Yes	Yes	Yes	Yes	Yes
        Sublime Text	Yes	Yes	Yes	Yes	Yes	Yes
        TED Notepad	No	No	No	Yes	Yes	No
        TextEdit	Yes	No	Yes	Yes	Yes	Yes
        TextMate	Yes	Yes	Partial	Yes	Yes	Yes
        TextPad	Yes	Yes	Yes	Yes	Yes	Yes
        TextWrangler	Yes	Yes	Yes	Yes	Yes	Yes
        The SemWare Editor	Yes	Yes	No	Yes	Yes	Yes
        UltraEdit	Yes	Limited	Yes	Yes	Yes	Yes
        VEDIT	Yes	Yes	Yes	Yes	Yes	Yes
        vi	No	Yes	No	No	No	No
        Vim	Yes	Yes	Yes	Yes	Yes	Yes
        Visual Studio Code	Plug-in	Yes	Yes	Yes	Yes	Yes
        Xeditor	Yes	Yes	Yes	Yes	Yes	Yes	
        XEmacs	Plug-in	Yes	Yes	Yes	Yes	Yes
Yi	?	Yes	?	?	Yes	Yes
    `;

    const tbody = $("#tbody").get(0);                   //jquery selector return an array like obj

    tbodyText.trim().split('\n').forEach((line) => {
        let colNum = 1;
        let cells = line.trim().split('\t').reduce((acc, cell, j, arr) => {
            if (arr[j+1] === arr[j]) {
                colNum++;
                return acc;
            } else {
                let theNum = colNum;
                colNum = 1;
                return acc + "<td colspan='" + theNum + "'>" + arr[j] + "</td>";
            }
        }, "");
        tbody.innerHTML += "<tr>" + cells + "</tr>";
    });
});
